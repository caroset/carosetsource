package com.sam.project.mvc.common;
 
import javax.sql.DataSource;
 
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
 
import com.alibaba.druid.pool.DruidDataSource;
 
/**
 * @ClassName: DataSourceConfiguration 
 * @Description: 单数据源配置
 */
@Configuration
@MapperScan(basePackages = "com.sam.project.*.mapper")
public class DataSourceConfiguration {
 
    @Bean
    @ConfigurationProperties(prefix = "jdbc.ds")
    public DataSource readDataSource() {
        return new DruidDataSource();
    }
}
