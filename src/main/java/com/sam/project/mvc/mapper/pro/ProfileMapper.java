package com.sam.project.mvc.mapper.pro;

import com.sam.project.mvc.model.Profile;

/**
 * @ClassName: ProfileMapper
 * @Description: mybites数据查询接口
 */
public interface ProfileMapper {
    int deleteByPrimaryKey(Profile profile);

    int insert(Profile profile);

    int insertSelective(Profile profile);

    Profile selectByPrimaryKey(Profile profile);

    int updateByPrimaryKeySelective(Profile profile);

    int updateByPrimaryKey(Profile profile);

    Profile selectByMemberNo(Profile profile);
}