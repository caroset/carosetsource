package com.sam.project.mvc.mapper.per;

import com.sam.project.mvc.model.Licenseocr;

/**
 * @ClassName: LicenseocrMapper
 * @Description: mybites数据查询接口
 */
public interface LicenseocrMapper {
    int deleteByPrimaryKey(Licenseocr key);

    int insert(Licenseocr record);

    int insertSelective(Licenseocr record);

    Licenseocr selectByPrimaryKey(Licenseocr key);

    int updateByPrimaryKeySelective(Licenseocr record);

    int updateByPrimaryKey(Licenseocr record);
}