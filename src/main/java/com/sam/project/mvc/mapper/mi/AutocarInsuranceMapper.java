package com.sam.project.mvc.mapper.mi;

import com.sam.project.mvc.model.AutocarInsurance;

/**
 * @ClassName: AutocarInsuranceMapper
 * @Description: mybites数据查询接口
 */
public interface AutocarInsuranceMapper {
    int deleteByPrimaryKey(Integer carId);

    int insert(AutocarInsurance record);

    int insertSelective(AutocarInsurance record);

    AutocarInsurance selectByPrimaryKey(Integer carId);

    int updateByPrimaryKeySelective(AutocarInsurance record);

    int updateByPrimaryKey(AutocarInsurance record);

    AutocarInsurance selectByCarId(Integer carId);
}