package com.sam.project.mvc.mapper.proce;

import java.util.List;

import com.sam.project.mvc.model.ScratchPhoto;

/**
 * @ClassName: ScratchPhotoMapper
 * @Description: mybites数据查询接口
 */
public interface ScratchPhotoMapper {
    int deleteByPrimaryKey(Integer photoId);

    int insert(ScratchPhoto record);

    int insertSelective(ScratchPhoto record);

    ScratchPhoto selectByPrimaryKey(Integer photoId);

    int updateByPrimaryKeySelective(ScratchPhoto record);

    int updateByPrimaryKey(ScratchPhoto record);

    List<ScratchPhoto> selectByCarId(Integer carId);
}