package com.sam.project.mvc.mapper.mi;

import java.util.List;

import com.sam.project.mvc.model.InsuranceCompanyMaster;

/**
 * @ClassName: InsuranceCompanyMasterMapper
 * @Description: mybites数据查询接口
 */
public interface InsuranceCompanyMasterMapper {
    int deleteByPrimaryKey(Integer insuranceCompanyId);

    int insert(InsuranceCompanyMaster record);

    int insertSelective(InsuranceCompanyMaster record);

    InsuranceCompanyMaster selectByPrimaryKey(Integer insuranceCompanyId);

    int updateByPrimaryKeySelective(InsuranceCompanyMaster record);

    int updateByPrimaryKey(InsuranceCompanyMaster record);

    public List<InsuranceCompanyMaster>  selectAll();
}