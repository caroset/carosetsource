package com.sam.project.mvc.mapper;

import com.sam.project.mvc.model.Account;

/**
 * @ClassName: AccountMapper
 * @Description: mybites数据查询接口
 */
public interface AccountMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);
}