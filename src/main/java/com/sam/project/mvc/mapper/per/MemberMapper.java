package com.sam.project.mvc.mapper.per;

import com.sam.project.mvc.model.Member;

/**
 * @ClassName: MemberMapper
 * @Description: mybites数据查询接口
 */
public interface MemberMapper {
    int deleteByPrimaryKey(Member memberId);

    int insert(Member member);

    int insertSelective(Member member);

    Member selectByPrimaryKey(Member memberId);

    int updateByPrimaryKeySelective(Member member);

    int updateByPrimaryKey(Member member);
}