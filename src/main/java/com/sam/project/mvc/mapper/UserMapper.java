package com.sam.project.mvc.mapper;
 
import java.util.List;
 
import com.sam.project.mvc.model.User;
 
/**
 * @ClassName: UserMapper 
 * @Description: mybites数据查询接口
 */
public interface UserMapper {
 
	List<User> queryList();
 
	void save(User user);
 
	void batchDelete(Integer[] ids);
 
	void update(User user);
 
}
