package com.sam.project.mvc.annotations;

import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.sam.project.mvc.validator.TelNumberValidator;

@Retention(RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
@Documented
@Constraint(
        validatedBy = {TelNumberValidator.class}
)
public @interface TelNumber {
	String message() default "{com.sam.project.mvc.annotations.TelNumber}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
