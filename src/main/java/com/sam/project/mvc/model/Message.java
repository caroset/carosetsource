package com.sam.project.mvc.model;

import java.util.Date;

public class Message {
    private Integer messageId;

    private Integer memberId;

    private Integer loanId;

    private Integer booking;

    private Date sendMailDate;

    private String content;

    private String photo;

    private Integer type;

    private String openFlg;

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getBooking() {
        return booking;
    }

    public void setBooking(Integer booking) {
        this.booking = booking;
    }

    public Date getSendMailDate() {
        return sendMailDate;
    }

    public void setSendMailDate(Date sendMailDate) {
        this.sendMailDate = sendMailDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOpenFlg() {
        return openFlg;
    }

    public void setOpenFlg(String openFlg) {
        this.openFlg = openFlg == null ? null : openFlg.trim();
    }
}