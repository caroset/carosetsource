package com.sam.project.mvc.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.sam.project.mvc.annotations.FileExist;
import com.sam.project.mvc.util.Items;

public class ScratchPhoto extends Items {

	@NotNull
	@Length(min=10,max=10)
    private Integer photoId;

	@NotNull
	@Length(min=10,max=10)
    private Integer carId;

	@NotNull
    private String scratchPlace;

	@FileExist
    private String photo;

	@NotNull
    private String reservationFlg;

    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public String getScratchPlace() {
        return scratchPlace;
    }

    public void setScratchPlace(String scratchPlace) {
        this.scratchPlace = scratchPlace == null ? null : scratchPlace.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public String getReservationFlg() {
        return reservationFlg;
    }

    public void setReservationFlg(String reservationFlg) {
        this.reservationFlg = reservationFlg == null ? null : reservationFlg.trim();
    }
}