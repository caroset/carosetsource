package com.sam.project.mvc.model;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.sam.project.mvc.util.Items;

public class Share extends Items {

	@NotNull
	@Length(min=10,max=10)
    private Integer shareId;

	@NotNull
	@Length(min=10,max=10)
    private Integer memberId;

	@NotNull
    private Integer state;

	@NotNull
	@Length(min=10,max=10)
    private Integer carId;

	@NotNull
    private Date startDate;

	@NotNull
    private Date endDate;

	@NotNull
    private Date bespeakTime;

	@NotNull
	@Min(value=1)
	@Max(value=10)
    private Integer bespeakDays;

    private Integer useMaybeDays;
    @NotNull
	@Min(value=1)
	@Max(value=10)
    private Integer lastloanDays;

    private String cancelReason;

    private String endDivision;

    private String bespeakMemo;

    private String injuryLoginEndFlg;

    private String myOkFlg;

    private String mycarInjuryOkFlg;

    private String mycarInjuryReturnFlg;

    private String actualFlg;

    public Integer getShareId() {
        return shareId;
    }

    public void setShareId(Integer shareId) {
        this.shareId = shareId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getBespeakTime() {
        return bespeakTime;
    }

    public void setBespeakTime(Date bespeakTime) {
        this.bespeakTime = bespeakTime;
    }

    public Integer getBespeakDays() {
        return bespeakDays;
    }

    public void setBespeakDays(Integer bespeakDays) {
        this.bespeakDays = bespeakDays;
    }

    public Integer getUseMaybeDays() {
        return useMaybeDays;
    }

    public void setUseMaybeDays(Integer useMaybeDays) {
        this.useMaybeDays = useMaybeDays;
    }

    public Integer getLastloanDays() {
        return lastloanDays;
    }

    public void setLastloanDays(Integer lastloanDays) {
        this.lastloanDays = lastloanDays;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason == null ? null : cancelReason.trim();
    }

    public String getEndDivision() {
        return endDivision;
    }

    public void setEndDivision(String endDivision) {
        this.endDivision = endDivision == null ? null : endDivision.trim();
    }

    public String getBespeakMemo() {
        return bespeakMemo;
    }

    public void setBespeakMemo(String bespeakMemo) {
        this.bespeakMemo = bespeakMemo == null ? null : bespeakMemo.trim();
    }

    public String getInjuryLoginEndFlg() {
        return injuryLoginEndFlg;
    }

    public void setInjuryLoginEndFlg(String injuryLoginEndFlg) {
        this.injuryLoginEndFlg = injuryLoginEndFlg == null ? null : injuryLoginEndFlg.trim();
    }

    public String getMyOkFlg() {
        return myOkFlg;
    }

    public void setMyOkFlg(String myOkFlg) {
        this.myOkFlg = myOkFlg == null ? null : myOkFlg.trim();
    }

    public String getMycarInjuryOkFlg() {
        return mycarInjuryOkFlg;
    }

    public void setMycarInjuryOkFlg(String mycarInjuryOkFlg) {
        this.mycarInjuryOkFlg = mycarInjuryOkFlg == null ? null : mycarInjuryOkFlg.trim();
    }

    public String getMycarInjuryReturnFlg() {
        return mycarInjuryReturnFlg;
    }

    public void setMycarInjuryReturnFlg(String mycarInjuryReturnFlg) {
        this.mycarInjuryReturnFlg = mycarInjuryReturnFlg == null ? null : mycarInjuryReturnFlg.trim();
    }

    public String getActualFlg() {
        return actualFlg;
    }

    public void setActualFlg(String actualFlg) {
        this.actualFlg = actualFlg == null ? null : actualFlg.trim();
    }
}