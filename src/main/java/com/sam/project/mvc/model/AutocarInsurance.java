package com.sam.project.mvc.model;

import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.sam.project.mvc.annotations.FileExist;
import com.sam.project.mvc.util.Items;

public class AutocarInsurance extends Items{

	@NotNull
	@Length(min=10,max=10)
    private int carId;

	@NotNull
	@Length(min=10,max=10)
    private int insuranceCardId;

	@NotNull
	@FileExist
    private String insuranceCardPhoto;

	@NotNull
    private Integer insuranceKaisyaId;

	@NotNull
	@Past
    private Date insuranceStartDate;

	@NotNull
	@Future
    private Date insuranceEndDate;

	@NotNull
    private String contractorName;

	@NotNull
	@Pattern(regexp = "[A-Z]{3}[0-9]{9}")
    private String insuranceNumber;

    private String contractorNameRelation;

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getInsuranceCardId() {
        return insuranceCardId;
    }

    public void setInsuranceCardId(Integer insuranceCardId) {
        this.insuranceCardId = insuranceCardId;
    }

    public String getInsuranceCardPhoto() {
        return insuranceCardPhoto;
    }

    public void setInsuranceCardPhoto(String insuranceCardPhoto) {
        this.insuranceCardPhoto = insuranceCardPhoto == null ? null : insuranceCardPhoto.trim();
    }

    public Integer getInsuranceKaisyaId() {
        return insuranceKaisyaId;
    }

    public void setInsuranceKaisyaId(Integer insuranceKaisyaId) {
        this.insuranceKaisyaId = insuranceKaisyaId;
    }

    public Date getInsuranceStartDate() {
        return insuranceStartDate;
    }

    public void setInsuranceStartDate(Date insuranceStartDate) {
        this.insuranceStartDate = insuranceStartDate;
    }

    public Date getInsuranceEndDate() {
        return insuranceEndDate;
    }

    public void setInsuranceEndDate(Date insuranceEndDate) {
        this.insuranceEndDate = insuranceEndDate;
    }

    public String getContractorName() {
        return contractorName;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName == null ? null : contractorName.trim();
    }

    public String getInsuranceNumber() {
        return insuranceNumber;
    }

    public void setInsuranceNumber(String insuranceNumber) {
        this.insuranceNumber = insuranceNumber;
    }

    public String getContractorNameRelation() {
        return contractorNameRelation;
    }

    public void setContractorNameRelation(String contractorNameRelation) {
        this.contractorNameRelation = contractorNameRelation == null ? null : contractorNameRelation.trim();
    }
}