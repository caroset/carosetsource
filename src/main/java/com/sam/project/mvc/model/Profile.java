package com.sam.project.mvc.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.sam.project.mvc.annotations.FileExist;
import com.sam.project.mvc.util.Items;

public class Profile  extends Items{
	//key
	@NotNull
	@Length(min=10,max=10)
    private Integer profileId;
	@NotNull
	@Length(min=10,max=10)
    private Integer memberId;
	@NotNull
	@Length(min=10,max=10)
    private Integer userId;

    public Integer getProfileId()  {
        return profileId;
    }

    public void setProfileId(Integer profileId) {
        this.profileId = profileId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
	//key
    private String effectiveflg;

    private String username;

    private String sex;

    @FileExist
    private String photo;

    private String drivingFre;

    private String introduction;

    public String getEffectiveflg() {
        return effectiveflg;
    }

    public void setEffectiveflg(String effectiveflg) {
        this.effectiveflg = effectiveflg == null ? null : effectiveflg.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public String getDrivingFre() {
        return drivingFre;
    }

    public void setDrivingFre(String drivingFre) {
        this.drivingFre = drivingFre == null ? null : drivingFre.trim();
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }
}