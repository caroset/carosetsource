
package com.sam.project.mvc;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;



/**

 * @ClassName: Application

 * @Description: springboot启动器

 */

// 开启缓存

@EnableCaching

@SpringBootApplication

public class Application extends SpringBootServletInitializer {



	@Override

	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

		return application.sources(Application.class);

	}



	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
		System.out.println("aaaaaaaaaaaaaaaaaaa");
		System.out.println("java.class.path:"+System.getProperties().getProperty("java.class.path"));
		System.out.println("aaaaaaaaaaaaaaaaaaa");
	}

}
