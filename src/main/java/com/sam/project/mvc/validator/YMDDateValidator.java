package com.sam.project.mvc.validator;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.sam.project.mvc.annotations.YMDDate;

public class YMDDateValidator implements ConstraintValidator<YMDDate,Object> {

	@Override
	public void initialize(YMDDate constraintAnnotation) {


	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		String regex ="^[1-9]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$";
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(String.valueOf(value));
		if(m.find()) {
			return true;
		}else {
			return false;
		}
	}


}
