package com.sam.project.mvc.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.sam.project.mvc.annotations.TelNumber;

public class TelNumberValidator implements ConstraintValidator<TelNumber,Object>{

	@Override
	public void initialize(TelNumber constraintAnnotation) {


	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		String regex ="[0-9{11}]";
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(String.valueOf(value).replace("-", ""));
		if(m.find()) {
			return true;
		}else {
			return false;
		}
	}

}
