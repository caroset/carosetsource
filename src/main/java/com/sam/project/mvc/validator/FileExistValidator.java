package com.sam.project.mvc.validator;

import java.io.File;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.sam.project.mvc.annotations.FileExist;

public class FileExistValidator implements ConstraintValidator<FileExist,Object>{

	@Override
	public void initialize(FileExist constraintAnnotation) {

	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		String path=(String) value;
		File file=new File(path);
		//パス下のファイルを取得する
		if(file.isFile())
			return true;
		//ファイルが存在しない場合
		return false;
	}

}
