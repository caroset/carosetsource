package com.sam.project.mvc.util;

import java.util.Date;
import java.util.Random;

/**
*
* @author kang.lei
* @ClassName: PasswordUtil
* @Description: パスワード作成
*/
public class PasswordUtil {


	   public final static String[] word={
	   "a","b","c","d","e","f",
	   "g","h","j","k","m","n",
	   "p","q","r","s","t","u",
	   "v","w","x","y","z",
	   "A","B","C","D","E","F",
	   "G","H","J","K","M","N",
	   "P","Q","R","S","T","U",
	   "V","W","X","Y","Z",",",".","#","$","&","'","\"","(",")","!","+","*","{","}","<",">","?","_","-","="
	   ,"//","^","@","~","`","\\","|"};

	   public static String[] num={
	    "0","1","2","3","4","5","6","7","8","9"
	   };
	   /**
	    * パスワードを作成する
	    * @return
	    */
	   public static String randomPassword(){

		   //パスワードの長さを設定する
		   int passwdLength=8;
	      StringBuffer stringBuffer=new StringBuffer();
	      //今の時刻によって ランダム数を取得する
	      Random random=new  Random(new Date().getTime());
	      // パスワードのランダム数を生成する
	      for(int i=0;i<passwdLength;i++) {
	    	  int randomInt = random.nextInt(2);
	    	  if(randomInt==0) {
	    		  stringBuffer.append(word[random.nextInt(word.length)]);
	    	  }else {
	    		  stringBuffer.append(num[random.nextInt(num.length)]);
	    	  }
	      }
	      return stringBuffer.toString();
	   }

	   public static void main(String[] args) {
	   System.out.println(PasswordUtil.randomPassword());
	}
}
