/**
 *
 */
package com.sam.project.mvc.service.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.res.ShareMapper;
import com.sam.project.mvc.model.Share;

/**
 * @author kang.lei
 * @Description サービス層の作成する
 */
@Service
public class ShareService {
	@Autowired
	private ShareMapper  shareMapper;

	/**
	 *
	 * @param share ストレージモデル
	 * @return インサート件数
	 */
	public int insertSelective(Share share) {
		return shareMapper.insertSelective(share);
	}


	/**
	 *
	 * @param share ストレージモデル
	 * @return 更新件数
	 */
	public int updateByPrimaryKeySelective(Share share) {
		return shareMapper.updateByPrimaryKeySelective(share);
	}

	/**
	 *
	 * @param share ストレージモデル
	 * @return 検索の結果
	 */
	public Share selectByPrimaryKey(Integer share) {
		return shareMapper.selectByPrimaryKey(share);
	}


}
