/**
 *
 */
package com.sam.project.mvc.service.mi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.mi.AutocarInsuranceMapper;
import com.sam.project.mvc.model.AutocarInsurance;
import com.sam.project.mvc.util.ItemsComponent;

/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class AutocarInsuranceService {

	@Autowired
	private AutocarInsuranceMapper autocarInsuranceMapper;
	/**
	 *
	 * @param autocarInsurance
	 * @Description 項目のインサート
	 * @return 検索件数
	 */
	public int insertSelective(AutocarInsurance autocarInsurance) {
		ItemsComponent.setItems(autocarInsurance);
		return autocarInsuranceMapper.insertSelective(autocarInsurance);
	}
	/**
	 *
	 * @param carId
	 * @Description 車IDによると 自動車保険のデータを取得しました
	 * @return AutocarInsurance データ
	 */
	public AutocarInsurance selectByCarId(Integer carId){
		return autocarInsuranceMapper.selectByCarId(carId);

	}

	/**
	 *
	 * @param record 自動車保険モデル
	 * @return 更新件数
	 *
	 */
	public int updateByPrimaryKeySelective(AutocarInsurance record) {

		return autocarInsuranceMapper.updateByPrimaryKeySelective(record);
	}



//	  int deleteByPrimaryKey(Integer carId);
//
//	    int insert(AutocarInsurance record);
//
//	    int insertSelective(AutocarInsurance record);
//
//	    AutocarInsurance selectByPrimaryKey(Integer carId);
//
//	    int updateByPrimaryKeySelective(AutocarInsurance record);
//
//	    int updateByPrimaryKey(AutocarInsurance record);



}
