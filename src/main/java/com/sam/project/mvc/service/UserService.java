package com.sam.project.mvc.service;
 
import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.mapper.UserMapper;
import com.sam.project.mvc.model.User;
 
@Service
public class UserService {
 
	@Autowired
	private UserMapper userMapper;
	
	public AjaxResult queryList() {
		List<User> list = userMapper.queryList();
		return new AjaxResult(list);
	}
 
	public AjaxResult save(User user) {
		user.setUsername("user" + System.currentTimeMillis());
		user.setPassword("123456");
		user.setEmail("user" + System.currentTimeMillis() + "@test.com");
		user.setUseable(1);
		userMapper.save(user);
		return new AjaxResult();
	}
 
	public AjaxResult batchDelete(Integer[] ids) {
		userMapper.batchDelete(ids);
		return new AjaxResult();
	}
 
	public AjaxResult update(User user) {
		userMapper.update(user);
		return new AjaxResult();
	}
 
}
