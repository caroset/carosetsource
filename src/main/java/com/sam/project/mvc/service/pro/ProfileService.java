/**
 *
 */
package com.sam.project.mvc.service.pro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.pro.ProfileMapper;
import com.sam.project.mvc.model.Profile;
import com.sam.project.mvc.util.ItemsComponent;

/**
 * @author kang.lei
 * @Description サービス層の作成する
 */
@Service
public class ProfileService {

	@Autowired
	private ProfileMapper profileMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param member プロフィール
	 */
	public void insert(Profile profile) {
		ItemsComponent.setItems(profile);
		profileMapper.insert(profile);
	}

	/**
	 * @Description モデルによると アカウントの一件データを更新しました
	 * @param member プロフィール
	 */
	public void updateByPrimaryKeySelective(Profile profile) {
		profileMapper.updateByPrimaryKeySelective(profile);
	}

	/**
	 * @Description モデルによると アカウントの一件データを検索しました
	 * @param member プロフィール
	 */
	public Profile selectByMemberNo(Profile profile) {
		return profileMapper.selectByMemberNo(profile);
	}

}
