package com.sam.project.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.AccountMapper;
import com.sam.project.mvc.model.Account;

@Service
public class LoginService {

	@Autowired
	private AccountMapper accountMapper;

	/**
	 * @Description 主キーによると 検索データを取得する
	 * @param id 主キー
	 * @return account アカウントデータ
	 */
	public Account selectByPrimaryKey(Integer id) {
		Account account = accountMapper.selectByPrimaryKey(id);
		return account;
	}
	/**
	 * @Description 主キーによると アカウントの一件データを更新しました
	 * @param id 主キー
	 */
	public void updateByPrimaryKeySelective(Account account) {
		 accountMapper.updateByPrimaryKeySelective(account);
	}

}
