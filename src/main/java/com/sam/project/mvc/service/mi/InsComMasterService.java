/**
 *
 */
package com.sam.project.mvc.service.mi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.mi.InsuranceCompanyMasterMapper;
import com.sam.project.mvc.model.InsuranceCompanyMaster;

/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class InsComMasterService {

	@Autowired
	private InsuranceCompanyMasterMapper insuranceCompanyMasterMapper;
	/**
	 *
	 * @return List<InsuranceCompanyMaster> 会社名のリスト
	 */
	public List<InsuranceCompanyMaster>  selectAll() {
		return insuranceCompanyMasterMapper.selectAll();
	}


//    int deleteByPrimaryKey(Integer insuranceCompanyId);
//
//    int insert(InsuranceCompanyMaster record);
//
//    int insertSelective(InsuranceCompanyMaster record);
//
//    InsuranceCompanyMaster selectByPrimaryKey(Integer insuranceCompanyId);
//
//    int updateByPrimaryKeySelective(InsuranceCompanyMaster record);
//
//    int updateByPrimaryKey(InsuranceCompanyMaster record);
}
