package com.sam.project.mvc.service.per;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.per.MemberMapper;
import com.sam.project.mvc.model.Member;
import com.sam.project.mvc.util.ItemsComponent;

/**
 * @author kang.lei
 * @Description サービス層を作成する
 */
@Service
public class MemberService {

	@Autowired
	private MemberMapper memberMapper;

	/**
	 * @Description モデルによると アカウントの一件データを挿入しました
	 * @param member 会員
	 */
	public void insert(Member member) {
		ItemsComponent.setItems(member);
		memberMapper.insert(member);
	}

	/**
	 * @Description 主キーによると アカウントの一件データを検索取得しました
	 * @param member 会員
	 */
	public Member selectByPrimaryKey(Member member) {

		return memberMapper.selectByPrimaryKey(member);
	}

	/**
	 * @Description 主キーによると アカウントのいくつか項目のデータを更新しました
	 * @param member 会員
	 */
	public int updateByPrimaryKeySelective(Member member) {

		return memberMapper.updateByPrimaryKeySelective(member);
	}
}
