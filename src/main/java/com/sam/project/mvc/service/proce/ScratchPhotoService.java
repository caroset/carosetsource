/**
 *
 */
package com.sam.project.mvc.service.proce;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.proce.ScratchPhotoMapper;
import com.sam.project.mvc.model.ScratchPhoto;
import com.sam.project.mvc.util.ItemsComponent;

/**
 * @author kang.lei
 * @Description サービス層の作成する
 */
@Service
public class ScratchPhotoService {

	@Autowired
	private ScratchPhotoMapper scratchPhotoMapper;
	/**
	 *
	 * @param scratchPhoto
	 * @return インサートデータ件数
	 */
	public int insertSelective(ScratchPhoto scratchPhoto) {
		ItemsComponent.setItems(scratchPhoto);
		return scratchPhotoMapper.insertSelective(scratchPhoto);
	}

	/**
	 *
	 * @param scratchPhoto
	 * @return 更新データ件数
	 */
	public int updateByPrimaryKeySelective(ScratchPhoto scratchPhoto) {
		return scratchPhotoMapper.updateByPrimaryKeySelective(scratchPhoto);
	}

	/**
	 *
	 * @param photoId
	 * @return 削除データ件数
	 */
	public int deleteByPrimaryKey(Integer photoId) {
		return scratchPhotoMapper.deleteByPrimaryKey(photoId);
	}

	/**
	 *
	 * @param carId
	 * @return 削除データ件数
	 */
	public List<ScratchPhoto> selectByCarId(Integer carId) {
		return scratchPhotoMapper.selectByCarId(carId);
	}
}
