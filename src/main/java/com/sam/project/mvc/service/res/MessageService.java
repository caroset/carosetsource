/**
 *
 */
package com.sam.project.mvc.service.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sam.project.mvc.mapper.res.MessageMapper;
import com.sam.project.mvc.model.Message;

/**
 * @author kang.lei
 * @Description サービス層の作成する
 */
@Service
public class MessageService {
	@Autowired
	private MessageMapper  MessageMapper;

	/**
	 *
	 * @param share ストレージモデル
	 * @return インサート件数
	 */
	public int insertSelective(Message message) {
		return MessageMapper.insertSelective(message);
	}


	/**
	 *
	 * @param messageId ストレージモデル
	 * @return 更新件数
	 */
	public int updateByPrimaryKeySelective(Message message) {
		return MessageMapper.updateByPrimaryKeySelective(message);
	}

	/**
	 *
	 * @param messageId
	 * @return 検索の結果
	 */
	public Message selectByPrimaryKey(Integer messageId) {
		return MessageMapper.selectByPrimaryKey(messageId);
	}


}
