package com.sam.project.mvc.controller.mi;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.AutocarInsurance;
import com.sam.project.mvc.model.InsuranceCompanyMaster;
import com.sam.project.mvc.service.mi.AutocarInsuranceService;
import com.sam.project.mvc.service.mi.InsComMasterService;

/**
*
* @author kang.lei
* @ClassName: EnrollInsController
* @Description: 入会保険
*/
@Controller
@RequestMapping("/enrollIns")
public class EnrollInsController extends BaseController<EnrollInsController>{

	@Autowired
	private AutocarInsuranceService  autocarInsuranceService;
	@Autowired
	private InsComMasterService insComMasterService;

	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public AjaxResult  login(@RequestBody @Valid AutocarInsurance autocarInsurance,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return new AjaxResult(bindingResult.getFieldError().getDefaultMessage());
		}
		// インサート動作をすると インサート件数を取得する
		Integer insertCount=autocarInsuranceService.insertSelective(autocarInsurance);
		// 正常に インサートした後で
		if(insertCount>0) {
			return new AjaxResult(autocarInsurance);
		}else {
			//上記以外の場合
			return new AjaxResult();
		}
	}

	@RequestMapping(value="/search",method=RequestMethod.POST)
	@ResponseBody
	public AjaxResult  search(String carId) {
		// 検索動作をすると 自動車保険データを取得する
		AutocarInsurance autocarInsuranceResult=autocarInsuranceService.selectByCarId(Integer.parseInt(carId));
		// 正常に 検索した後で
		if(autocarInsuranceResult!=null) {
			return new AjaxResult(autocarInsuranceResult);
		}else {
			//上記以外の場合
			return new AjaxResult();
		}
	}

	@RequestMapping(value="/update",method=RequestMethod.POST)
	@ResponseBody
	public AjaxResult  update(@RequestBody AutocarInsurance autocarInsurance) {
		// 更新動作をすると 更新件数を取得する
		Integer updateCount=autocarInsuranceService.updateByPrimaryKeySelective(autocarInsurance);
		// 正常に 更新した後で
		if(updateCount>0) {
			return new AjaxResult(autocarInsurance);
		}else {
			//上記以外の場合
			return new AjaxResult();
		}
	}

	@RequestMapping(value="/search/companyName",method=RequestMethod.POST)
	@ResponseBody
	public AjaxResult  companyName() {
		// 検索をしています
		List<InsuranceCompanyMaster> insComMasterServiceList =insComMasterService.selectAll();
		List<String> result = new ArrayList<>();
		//会社名をまとめる
		for(InsuranceCompanyMaster insuranceCompanyMaster:insComMasterServiceList) {
			result.add(insuranceCompanyMaster.getName());
		}

	return new AjaxResult(result);

	}

}
