/**
 *
 */
package com.sam.project.mvc.controller.per;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.Member;
import com.sam.project.mvc.service.per.MemberService;

/**
*
* @author kang.lei
* @ClassName: InfoRegistController
* @Description: 情報登録
*/
@Controller
public class InfoRegistController extends BaseController<InfoRegistController> {

	@Autowired
	private MemberService memberService;

	/**
	 *
	 * @Description: アカウントテーブルのインサートする
	 * @param member アカウントモデル
	 */
	@ResponseBody
	@RequestMapping(value = {"/memberLogin"}, method = RequestMethod.POST)
	public AjaxResult memberLogin(@RequestBody @Valid Member member,BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			return new AjaxResult(bindingResult.getFieldError().getDefaultMessage());
		}
	//会員IDを取得する
	Member memberLogin = member;
	memberLogin.setMemberId(1);

	//会員登録
	memberService.insert(memberLogin);
	return new AjaxResult(memberLogin);
	}

	/**
	 *
	 * @Description: アカウントテーブルの検索する
	 * @param member アカウントモデル
	 */
	@ResponseBody
	@RequestMapping(value = {"/memberSearch"}, method = RequestMethod.POST)
	public AjaxResult memberSearch(@RequestBody Member member) {

	//会員IDによると検索する
	Member memberdd=memberService.selectByPrimaryKey(member);
	return new AjaxResult(memberdd);
	}

	/**
	 *
	 * @Description: アカウントテーブルの更新する
	 * @param member アカウントモデル
	 */
	@ResponseBody
	@RequestMapping(value = {"/memberUpdate"}, method = RequestMethod.POST)
	public AjaxResult memberUpdate(@RequestBody Member member) {

	//会員IDによると更新する
	Integer memberdd=memberService.updateByPrimaryKeySelective(member);
	//更新すると 成功かどうかを判断する
	if(memberdd>0) {
		return new AjaxResult(member);
	}else {
		//上記以外の場合
		return new AjaxResult();

	}

	}


}
