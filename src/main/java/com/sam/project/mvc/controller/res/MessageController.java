/**
 *
 */
package com.sam.project.mvc.controller.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.Message;
import com.sam.project.mvc.service.res.MessageService;

/**
 * @author kang.lei
 * @ClassName: MessageController
 * @Description: メセッジ
 */
@Controller
@RequestMapping("/message")
public class MessageController extends BaseController<MessageController>  {

	@Autowired
	private MessageService messageService;

	/**
	 *
	 * @param share ストレージモデル
	 * @return 戻り値インサートした結果
	 */
	@ResponseBody
	@RequestMapping("/insert")
	public AjaxResult insert(Message message) {
		//インサートを実行する
		messageService.insertSelective(message);
		return new AjaxResult();
	}

	/**
	 *
	 * @param share ストレージモデル
	 * @return 戻り値インサートした結果
	 */
	@ResponseBody
	@RequestMapping("/update")
	public AjaxResult update(Message message) {
		//インサートを実行する
		messageService.updateByPrimaryKeySelective(message);
		return new AjaxResult();
	}

	/**
	 *
	 * @param share ストレージモデル
	 * @return 戻り値インサートした結果
	 */
	@ResponseBody
	@RequestMapping("/search")
	public AjaxResult update(Integer messageId) {
		//インサートを実行する
		Message messageResult=messageService.selectByPrimaryKey(messageId);
		return new AjaxResult(messageResult);
	}
}
