package com.sam.project.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.Account;
import com.sam.project.mvc.service.LoginService;

/**
 *
 * @author kang.lei
 * @ClassName: login
 * @Description: ログイン
 */
@Controller
public class LoginController extends  BaseController<LoginController>{
	@Autowired
	private LoginService loginService;

	/**
	 *
	 * @Description: ログインの時 アカウントの一致性をチェックする
	 */
	@ResponseBody
	@RequestMapping("/loginCheck")
	public AjaxResult loginCheck(Account account) {

	//ユーゼIDを取得する
	Integer userId = account.getUserId();
	//アカウント情報を取得する
	Account accountCheck=this.loginService.selectByPrimaryKey(userId);
	//メールが一致の場合
	if(accountCheck!=null &&accountCheck.getMail().equals(account.getMail())) {
		return new AjaxResult(accountCheck);
	}
	return new AjaxResult("メールログインの時　不一致性があります");
	}
}

