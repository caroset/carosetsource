package com.sam.project.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.util.PasswordUtil;

/**
*
* @author kang.lei
* @ClassName: PasswordCreate
* @Description: 初期パスワードの作成
*/
@Controller
public class PasswordCreate  extends BaseController<LoginController>{


	@ResponseBody
	@RequestMapping("/passwdCreate")
	public String passwdCreate() {
		return PasswordUtil.randomPassword();
	}

}
