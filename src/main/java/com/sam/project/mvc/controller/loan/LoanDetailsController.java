/**
 *
 */
package com.sam.project.mvc.controller.loan;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.project.mvc.core.controller.BaseController;

/**
 * @author kang.lei
 * @ClassName: LoanDetailsController
 * @Description: 貸出詳細コントローラ
 */
@Controller
@RequestMapping("lodt")
public class LoanDetailsController extends BaseController<LoanDetailsController>{

}
