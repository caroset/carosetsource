/**
 *
 */
package com.sam.project.mvc.controller.pro;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.Profile;
import com.sam.project.mvc.service.pro.ProfileService;

/**
 * @author kang.lei
* @ClassName: ProfileController
* @Description: プロフィール
 */
@Controller
@RequestMapping("/profile")
public class ProfileController extends BaseController<ProfileController> {

	@Autowired
	private ProfileService  profileService;

	/**
	 *
	 * @Description: プロフィールテーブルのインサートする
	 * @param Profile プロフィールモデル
	 */
	@ResponseBody
	@RequestMapping(value = {"/login"}, method = RequestMethod.POST)
	public AjaxResult login(@RequestBody @Valid Profile profile,BindingResult bindingResult) {
		if(bindingResult.hasErrors()) {
			return new AjaxResult(bindingResult.getFieldError().getDefaultMessage());
		}
	//プロフィールを取得する
	Profile profileLogin = profile;

	//プロフィール登録
	profileService.insert(profileLogin);
	return new AjaxResult(profileLogin);
	}

	/**
	 *
	 * @Description: プロフィールテーブルの更新するする
	 * @param Profile プロフィールモデル
	 */
	@ResponseBody
	@RequestMapping(value = {"/update"}, method = RequestMethod.POST)
	public AjaxResult update(@RequestBody Profile profile) {

	//プロフィールを取得する
	Profile profileLogin = profile;

	//プロフィール更新
	profileService.updateByPrimaryKeySelective(profileLogin);
	return new AjaxResult(profileLogin);
	}

	/**
	 *
	 * @Description: プロフィールテーブルの更新するする
	 * @param Profile プロフィールモデル
	 */
	@ResponseBody
	@RequestMapping(value = {"/search"}, method = RequestMethod.POST)
	public AjaxResult search(@RequestBody Profile profile) {

	//プロフィールを取得する
	Profile profileLogin = profile;

	//プロフィール検索
	Profile result =profileService.selectByMemberNo(profileLogin);
	return new AjaxResult(result);
	}

}
