/**
 *
 */
package com.sam.project.mvc.controller.srt;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sam.project.mvc.core.controller.BaseController;

/**
 * @author kang.lei
* @ClassName: SearchTopController
* @Description: 検索トップ
 */
@Controller
@RequestMapping("/searchTop")
public class SearchTopController extends BaseController<SearchTopController>{


}
