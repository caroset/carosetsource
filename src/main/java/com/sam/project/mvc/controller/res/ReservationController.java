/**
 *
 */
package com.sam.project.mvc.controller.res;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.Share;
import com.sam.project.mvc.service.res.ShareService;

/**
 * @author kang.lei
 * @ClassName: ReservationController
 * @Description: 予約
 */
@Controller
@RequestMapping("/reservate")
public class ReservationController extends BaseController<ReservationController>  {

	@Autowired
	private ShareService shareService;

	/**
	 *
	 * @param share ストレージモデル
	 * @return 戻り値インサートした結果
	 */
	@ResponseBody
	@RequestMapping("/insert")
	public AjaxResult insert(Share share) {
		//インサートを実行する
		shareService.insertSelective(share);
		return new AjaxResult();
	}

	/**
	 *
	 * @param share ストレージモデル
	 * @return 戻り値インサートした結果
	 */
	@ResponseBody
	@RequestMapping("/update")
	public AjaxResult update(Share share) {
		//インサートを実行する
		shareService.updateByPrimaryKeySelective(share);
		return new AjaxResult();
	}

	/**
	 *
	 * @param share ストレージモデル
	 * @return 戻り値インサートした結果
	 */
	@ResponseBody
	@RequestMapping("/search")
	public AjaxResult update(Integer share) {
		//インサートを実行する
		Share shareResult=shareService.selectByPrimaryKey(share);
		return new AjaxResult(shareResult);
	}
}
