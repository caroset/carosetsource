/**
 *
 */
package com.sam.project.mvc.controller.proce;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.ScratchPhoto;
import com.sam.project.mvc.service.proce.ScratchPhotoService;

/**
 * @author kang.lei
 * @ClassName: ScratchController
 * @Description: 傷写真
 */
@Controller
@RequestMapping("/scra")
public class ScratchController  extends BaseController<ScratchController>{

	@Autowired
	private ScratchPhotoService scratchPhotoService;
	/**
	 * @Description 傷写真の新規登録
	 * @param  scratchPhoto 傷写真モデル
	 * @return AjaxResult
	 */
	@RequestMapping("/insert")
	@ResponseBody
	public AjaxResult insert(@RequestBody @Valid ScratchPhoto scratchPhoto,BindingResult bindingResult) {

		if(bindingResult.hasErrors()) {
			return new AjaxResult(bindingResult.getFieldError().getDefaultMessage());
		}
		//傷を登録する
		int insertCount=scratchPhotoService.insertSelective(scratchPhoto);
		//正常にすることを検証する
		if(insertCount>0) {
			return new AjaxResult(scratchPhoto);
		}else {
			//上記以外の場合
			return new AjaxResult();
		}

	}

	/**
	 * @Description 傷写真の編集
	 * @param  scratchPhoto 傷写真モデル
	 * @return AjaxResult
	 */
	@RequestMapping("/update")
	@ResponseBody
	public AjaxResult update(@RequestBody ScratchPhoto scratchPhoto) {

		//傷を編集する
		int insertCount=scratchPhotoService.updateByPrimaryKeySelective(scratchPhoto);
		//正常にすることを検証する
		if(insertCount>0) {
			return new AjaxResult(scratchPhoto);
		}else {
			//上記以外の場合
			return new AjaxResult();
		}

	}

	/**
	 * @Description 傷写真の削除
	 * @param  scratchPhoto 傷写真モデル
	 * @return AjaxResult
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public AjaxResult delete(Integer photoId) {

		//傷を登録する
		int insertCount=scratchPhotoService.deleteByPrimaryKey(photoId);
		//正常にすることを検証する
		if(insertCount>0) {
			return new AjaxResult(photoId);
		}else {
			//上記以外の場合
			return new AjaxResult();
		}

	}

	/**
	 * @Description 確認事項
	 * @param  scratchPhoto 傷写真モデル
	 * @return AjaxResult
	 */
	@RequestMapping("/search")
	@ResponseBody
	public AjaxResult search(Integer carId) {

		//傷を登録する
		List<ScratchPhoto> scratchPhotoList=scratchPhotoService.selectByCarId(carId);
		//戻り値を設定する
		List<Map<String,Object>> resultList = new ArrayList<>();
		Map<String,Object> map;
		for(int i=0;i<scratchPhotoList.size();i++) {
			map= new HashMap<>();
			map.put("photoId", scratchPhotoList.get(i).getPhotoId());
			map.put("scratchPlace", scratchPhotoList.get(i).getScratchPlace());
			map.put("photo", scratchPhotoList.get(i).getPhoto());
			resultList.add(map);
		}
		//戻り値を戻す
		return new AjaxResult(resultList);


	}
}
