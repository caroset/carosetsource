package com.sam.project.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.model.User;
import com.sam.project.mvc.service.UserService;

/**
 * @ClassName: UserController
 * @Description: 用户Controller
 */
@Controller
public class UserController {

	@Autowired
	private UserService userService;

	@ResponseBody
	@RequestMapping("/queryList")
	public AjaxResult queryList(){
		return userService.queryList();
	}

	@ResponseBody
	@RequestMapping("/addUser")
	public AjaxResult addUser(User user){
		System.out.println(user.getId());
		return userService.save(user);
	}

	@ResponseBody
	@RequestMapping("/delUser")
	public AjaxResult delUser(Integer[] ids){
		return userService.batchDelete(ids);
	}

	@ResponseBody
	@RequestMapping("/updateUser")
	public AjaxResult updateUser(User user){
		return userService.update(user);
	}

}
