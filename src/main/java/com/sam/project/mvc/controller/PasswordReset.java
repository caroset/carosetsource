package com.sam.project.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sam.project.mvc.common.AjaxResult;
import com.sam.project.mvc.core.controller.BaseController;
import com.sam.project.mvc.model.Account;
import com.sam.project.mvc.service.LoginService;

/**
*
* @author kang.lei
* @ClassName: PasswordReset
* @Description: ログインの時 パスワードをわすれた　パスワードをリセットする
*/
@Controller
public class PasswordReset extends BaseController<LoginController> {

	@Autowired
	LoginService loginService;

	@ResponseBody
	@RequestMapping("/pdReset")
	public AjaxResult pdReset(Account account) {
		// 主キーユーゼIDを判断すると 存在しない場合
		if(account==null|| account.getUserId()==null)
			return new AjaxResult();
		loginService.updateByPrimaryKeySelective(account);
		return new AjaxResult();
	}

}
